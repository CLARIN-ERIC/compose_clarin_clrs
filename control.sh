#!/bin/bash

set -e

shopt -s expand_aliases
[[ -f ~/.bashrc ]] && . ~/.bashrc

NGINX_HOST=nginx

STOP=0
START=0
VOLUME=0
HELP=0
VERBOSE=0

container_is_running ( ) {
    if ! (cd "$1" && docker-compose ps "$2" |grep -q "Up "); then
        return 1
    else
        return 0
    fi
}

container_is_healthy ( ) {
    # $1 : Docker compose file location
    # $2 : Service name
    if ! (cd "$1" && docker-compose ps "$2" |grep -q "(healthy)"); then
        return 1
    else
        return 0
    fi
}

get_container_name ( ) {
    # $1 : Docker compose file location
    # $2 : Service name
    (cd "$1"
    echo $(docker-compose images "$2" | grep "$2" | awk '{ print $1 }'))
}

#
# Process script arguments
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    stop)
        STOP=1
        ;;
    start)
        START=1
        ;;
    restart)
        STOP=1
        START=1
        ;;
    -V|--del-volumes)
        VOLUME=1
        ;;
    -h|--help)
        HELP=1
       ;;
    -d|--debug)
        VERBOSE=1
        ;;
    *)
        echo "Unkown option: $key"
        HELP=1
        ;;
esac
shift # past argument or value
done

# Print parameters if running in verbose mode
if [ ${VERBOSE} -eq 1 ]; then
    set -x
fi

#
# Execute based on mode argument
#
if [ ${HELP} -eq 1 ]; then
    echo ""
    echo "control.sh [start|stop|restart] [-hd]"
    echo ""
    echo "  start       Start Language Resource Switchboard"
    echo "  stop        Stop Language Resource Switchboard"
    echo "  restart     Restart Language Resource Switchboard"
    echo ""
    echo "  -V, --del-volumes [start|stop|restart]      Remove all volumes."
    echo "  -d, --debug Run this script in verbose mode"
    echo ""
    echo "  -h, --help  Show help"
    echo ""
    exit 0
else

    COMPOSE_OPTS=""
    if [ ${VOLUME} -eq 1 ]; then
        COMPOSE_OPTS="--volumes"
        STOP=1
    fi

    COMPOSE_DIR="clarin"
    if [ $(readlink $0) ]; then
        COMPOSE_DIR=$(dirname $(readlink $0))/$COMPOSE_DIR
    else
        COMPOSE_DIR=$(dirname $BASH_SOURCE)/$COMPOSE_DIR
    fi

    if [ ${STOP} -eq 1 ]; then
        (cd $COMPOSE_DIR && docker-compose down $COMPOSE_OPTS)
    fi
    if [ ${START} -eq 1 ]; then
        if [ $(docker network ls |grep postfix_mail|wc -l)  -eq 0 ]; then
            docker network create postfix_mail
        fi
        cd $COMPOSE_DIR && docker-compose up -d
        while ! container_is_healthy . ${NGINX_HOST}
        do
            echo "Waiting for ${NGINX_HOST} container to become available"
            sleep 5
        done

        # configure Nexcloud instance
        if ! docker-compose exec --user www-data nextcloud-fpm sh -c "php occ user:setting switchboard |grep -q 'settings:'"; then
            docker-compose exec --user root nextcloud-fpm sh -c "apk add bash"

            # Restore empty database with initial configurations. Retention and workflow settings are only stored in the database and there is no occ command to set them
            docker cp ../database/nextcloud.db $(get_container_name . nextcloud-fpm):/var/www/html/data/nextcloud.db
            docker-compose exec --user root nextcloud-fpm bash -c "chown www-data:www-data /var/www/html/data/nextcloud.db"

            ## configure switchboard user
            docker-compose exec --user www-data nextcloud-fpm bash -c "export OC_PASS=\$NEXTCLOUD_ADMIN_PASSWORD;php occ user:resetpassword switchboard --password-from-env"
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ user:setting switchboard firstrunwizard show 0"
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ user:setting switchboard core locale 'en'"
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ user:setting switchboard core lang 'nl_NL'"
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ user:setting switchboard core timezone 'Europe/Berlin'"
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ user:setting switchboard settings email \$NEXTCLOUD_ADMIN_EMAIL"
            ## configure internal mail
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ config:system:set mail_from_address --value 'nextcloud'"
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ config:system:set mail_domain --value 'switchboard-clarin.eu'"
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ config:system:set mail_smtpmode --value 'smtp'"
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ config:system:set mail_smtphost --value 'mail'"
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ config:system:set mail_smtpport --value '25'"
            ## configure for running behind a proxy
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ config:system:set overwritewebroot --value /nextcloud"
            ## update database 
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ db:convert-filecache-bigint -n"
            ## use background cron job
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ background:cron"
            ## Disable survey and federation apps. Install and enable files_retention, files_accesscontrol and files_automatedtagging
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ app:disable survey_client"
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ app:disable federation"
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ app:enable admin_audit"
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ app:install files_retention"
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ app:install files_accesscontrol"
            docker-compose exec --user www-data nextcloud-fpm bash -c "php occ app:install files_automatedtagging"
        fi
    fi
fi
