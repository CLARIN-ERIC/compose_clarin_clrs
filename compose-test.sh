#!/bin/bash

set -e

PRECOMPILED_BUILD_IMAGE="registry.gitlab.com/clarin-eric/build-image:1.1.0"
BUILD_IMAGE=${PRECOMPILED_BUILD_IMAGE}

#
# Set default values for parameters
#
MODE="gitlab"
TEST=0
VERBOSE=0

#
# Process script arguments
#
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -h|--help)
        MODE="help"
        ;;
    -l|--local)
        MODE="local"
        ;;
    -t|--test)
        TEST=1
        ;;
    -v|--verbose)
        VERBOSE=1
        ;;
    *)
        echo "Unkown option: $key"
        MODE="help"
        ;;
esac
shift # past argument or value
done

# Source variables if it exists
if [ -f variables.sh ]; then
    . ./variables.sh
    set +x
fi

# Print parameters if running in verbose mode
if [ ${VERBOSE} -eq 1 ]; then
    set -x
fi

#
# Execute based on mode argument
#
if [ ${MODE} == "help" ]; then
    echo ""
    echo "compose-test.sh [-tlvh]"
    echo ""
    echo "  -t, --test       Execute tests"
    echo ""
    echo "  -l, --local      Run workflow locally in a local docker container"
    echo "  -v, --verbose    Run in verbose mode"
    echo ""
    echo "  -h, --help       Show help"
    echo ""
    exit 0
elif [ "${MODE}" == "gitlab" ]; then
    #Test
    if [ "${TEST}" -eq 1 ]; then
        echo "**** Testing image *******************************"
        if [ ! -d 'test' ]; then
            echo "Test directory (./test/) not found"
            exit 1
        fi
        if [ ! -f 'test/docker-compose.yml' ]; then
            echo "docker-compose.yml not found in test directory (./test/docker-compose.yml)"
            exit 1
        fi
        cd -- 'test/'
        apk --quiet update --update-cache
        apk --quiet add 'py2-pip=9.0.0-r1'
        pip --quiet --disable-pip-version-check install 'docker-compose==1.8.0'
        
        ./run-test.sh
        number_of_failed_containers=$?
       
        #return result
        exit "$number_of_failed_containers"
    fi

elif [ "${MODE}" == "local" ]; then
    #Run
    if [ "${TEST}" -eq 1 ]; then
        #
        # Setup all commands
        #

        SHELL_FLAGS=""
        if [ ${VERBOSE} -eq 1 ]; then
            FLAGS="-x"
        fi

        FLAGS=""

        CMD="sh ${SHELL_FLAGS} ./compose-test.sh --test ${FLAGS}"

        #Start the test process
        docker run \
            --volume='/var/run/docker.sock:/var/run/docker.sock' \
            --rm \
            --volume="$PWD":"$PWD" \
            --env-file="./test/.env" \
            --workdir="$PWD" \
            -it \
            ${BUILD_IMAGE} \
            sh -c "${CMD}"
    fi
else
    exit 1
fi
