#!/bin/sh

docker-compose down -v
if [ $(docker network ls |grep postfix_mail|wc -l)  -eq 0 ]; then
    docker network create postfix_mail
fi
docker-compose up
#Verify all containers are closed nicely
number_of_failed_containers="$(docker-compose ps -q | xargs docker inspect \
	-f '{{ .State.ExitCode }}' | grep -c 0 -v | tr -d ' ')"
#cleanup
docker-compose down -v

exit "$number_of_failed_containers"